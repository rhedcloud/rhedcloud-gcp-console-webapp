package edu.emory.oit.vpcprovisioning.client.desktop;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.ViewImplBase;
import edu.emory.oit.vpcprovisioning.presenter.centraladmin.ListCentralAdminView;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class DesktopListCentralAdmin extends ViewImplBase implements ListCentralAdminView {
	Presenter presenter;
	private ListDataProvider<RoleAssignmentSummaryPojo> dataProvider = new ListDataProvider<RoleAssignmentSummaryPojo>();
	private SingleSelectionModel<RoleAssignmentSummaryPojo> selectionModel;
	List<RoleAssignmentSummaryPojo> centralAdminList = new java.util.ArrayList<RoleAssignmentSummaryPojo>();
	UserAccountPojo userLoggedIn;
    PopupPanel actionsPopup = new PopupPanel(true);

	/*** FIELDS ***/
	@UiField(provided=true) SimplePager centralAdminListPager = new SimplePager(TextLocation.RIGHT, false, true);
	@UiField Button actionsButton;
	@UiField(provided=true) CellTable<RoleAssignmentSummaryPojo> centralAdminListTable = new CellTable<RoleAssignmentSummaryPojo>(15, (CellTable.Resources)GWT.create(MyCellTableResources.class));
	@UiField VerticalPanel centralAdminListPanel;
	@UiField HorizontalPanel pleaseWaitPanel;
	@UiField HTML introBodyHTML;

//	@UiField Button homeButton;
//	@UiHandler("homeButton")
//	void homeButtonClicked(ClickEvent e) {
//		ActionEvent.fire(presenter.getEventBus(), ActionNames.GO_HOME);
//	}

	@UiField PushButton refreshButton;

	@UiHandler("refreshButton")
	void refreshButtonClicked(ClickEvent e) {
		presenter.refreshList(userLoggedIn);
	}

	public interface MyCellTableResources extends CellTable.Resources {

	     @Source({CellTable.Style.DEFAULT_CSS, "cellTableStyles.css" })
	     public CellTable.Style cellTableStyle();
	}
	
	private static DesktopListCentralAdminUiBinder uiBinder = GWT.create(DesktopListCentralAdminUiBinder.class);

	interface DesktopListCentralAdminUiBinder extends UiBinder<Widget, DesktopListCentralAdmin> {
	}

	public DesktopListCentralAdmin() {
		initWidget(uiBinder.createAndBindUi(this));
		setRefreshButtonImage(refreshButton);
	}

	@UiHandler("actionsButton")
	void actionsButtonClicked(ClickEvent e) {
		actionsPopup.clear();
		actionsPopup.setAutoHideEnabled(true);
		actionsPopup.setAnimationEnabled(true);
		actionsPopup.getElement().getStyle().setBackgroundColor("#f1f1f1");

		Grid grid = new Grid(2, 1);
		grid.setCellSpacing(8);
		actionsPopup.add(grid);

		Anchor userNotificationAnchor = new Anchor("Create User Notification");
		userNotificationAnchor.addStyleName("productAnchor");
		userNotificationAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		userNotificationAnchor.setTitle("Create a user notification");
		userNotificationAnchor.ensureDebugId(userNotificationAnchor.getText());
		userNotificationAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				RoleAssignmentSummaryPojo m = selectionModel.getSelectedObject();
				ActionEvent.fire(presenter.getEventBus(), ActionNames.CREATE_USER_NOTIFICATION, userLoggedIn);
			}
		});
		grid.setWidget(0, 0, userNotificationAnchor);

		Anchor accountNotificationAnchor = new Anchor("Create Account Notification");
		accountNotificationAnchor.addStyleName("productAnchor");
		accountNotificationAnchor.getElement().getStyle().setBackgroundColor("#f1f1f1");
		accountNotificationAnchor.setTitle("Create an account notification");
		accountNotificationAnchor.ensureDebugId(accountNotificationAnchor.getText());
		accountNotificationAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				actionsPopup.hide();
				RoleAssignmentSummaryPojo m = selectionModel.getSelectedObject();
				ActionEvent.fire(presenter.getEventBus(), ActionNames.CREATE_ACCOUNT_NOTIFICATION, userLoggedIn);
			}
		});
		grid.setWidget(1, 0, accountNotificationAnchor);

		actionsPopup.showRelativeTo(actionsButton);
	}

	@Override
	public void hidePleaseWaitPanel() {
		pleaseWaitPanel.setVisible(false);
	}

	@UiField HTML pleaseWaitHTML;
	@Override
	public void showPleaseWaitPanel(String pleaseWaitHTML) {
		if (pleaseWaitHTML == null || pleaseWaitHTML.length() == 0) {
			this.pleaseWaitHTML.setHTML("Please wait...");
		}
		else {
			this.pleaseWaitHTML.setHTML(pleaseWaitHTML);
		}
		this.pleaseWaitPanel.setVisible(true);
	}

	@Override
	public void setInitialFocus() {
		
		
	}

	@Override
	public Widget getStatusMessageSource() {
		
		return null;
	}

	@Override
	public void applyCentralAdminMask() {
		
		
	}

	@Override
	public void applyAWSAccountAdminMask() {
		
		
	}

	@Override
	public void applyAWSAccountAuditorMask() {
		
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public List<Widget> getMissingRequiredFields() {
		
		return null;
	}

	@Override
	public void resetFieldStyles() {
		
		
	}

	@Override
	public HasClickHandlers getCancelWidget() {
		
		return null;
	}

	@Override
	public HasClickHandlers getOkayWidget() {
		
		return null;
	}

	@Override
	public void vpcpPromptOkay(String valueEntered) {
		
		
	}

	@Override
	public void vpcpPromptCancel() {
		
		
	}

	@Override
	public void vpcpConfirmOkay() {
		
		
	}

	@Override
	public void vpcpConfirmCancel() {
		
		
	}

	@Override
	public void clearList() {
		centralAdminListTable.setVisibleRangeAndClearData(centralAdminListTable.getVisibleRange(), true);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setCentralAdmins(List<RoleAssignmentSummaryPojo> centralAdmins) {
		GWT.log("view Setting centralAdmins.");
		this.centralAdminList = centralAdmins;
		this.initializeCentralAdminListTable();
	    centralAdminListPager.setDisplay(centralAdminListTable);
	}

	private Widget initializeCentralAdminListTable() {
		GWT.log("initializing Central Admin list table...");
		centralAdminListTable.setTableLayoutFixed(false);
		centralAdminListTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
		
		// set range to display
		centralAdminListTable.setVisibleRange(0, 20);
		
		// create dataprovider
		dataProvider = new ListDataProvider<RoleAssignmentSummaryPojo>();
		dataProvider.addDataDisplay(centralAdminListTable);
		dataProvider.getList().clear();
		dataProvider.getList().addAll(this.centralAdminList);
		
		selectionModel = 
	    	new SingleSelectionModel<RoleAssignmentSummaryPojo>(RoleAssignmentSummaryPojo.KEY_PROVIDER);
		centralAdminListTable.setSelectionModel(selectionModel);
	    
	    selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
	    	@Override
	    	public void onSelectionChange(SelectionChangeEvent event) {
	    		RoleAssignmentSummaryPojo m = selectionModel.getSelectedObject();
//	    		GWT.log("Selected centralAdmin is: " + m.getAccountId());
	    	}
	    });

	    ListHandler<RoleAssignmentSummaryPojo> sortHandler = 
	    	new ListHandler<RoleAssignmentSummaryPojo>(dataProvider.getList());
	    centralAdminListTable.addColumnSortHandler(sortHandler);

	    if (centralAdminListTable.getColumnCount() == 0) {
		    initCentralAdminListTableColumns(sortHandler);
	    }
		
		return centralAdminListTable;
	}

	private void initCentralAdminListTableColumns(ListHandler<RoleAssignmentSummaryPojo> sortHandler) {
		GWT.log("initializing Central Admin list table columns...");
		
		Column<RoleAssignmentSummaryPojo, String> adminNameColumn = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				if (object.getDirectoryPerson() != null) {
					if (object.getDirectoryPerson().getFullName() == null || 
						object.getDirectoryPerson().getFullName().length() == 0) {

						return "Unknown name.  PPID is: " + object.getDirectoryPerson().getKey();
					}
					else {
						return object.getDirectoryPerson().getFullName();
					}
				}
				else {
					return "Unknown (null Directory Person)";
				}
			}
		};
		adminNameColumn.setSortable(true);
		adminNameColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(adminNameColumn, new Comparator<RoleAssignmentSummaryPojo>() {
			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
				return o1.getDirectoryPerson().getFullName().compareTo(o2.getDirectoryPerson().getFullName());
			}
		});
		centralAdminListTable.addColumn(adminNameColumn, "Admin Name");

		Column<RoleAssignmentSummaryPojo, String> reasonColumn = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				return object.getRoleAssignment().getReason();
			}
		};
		reasonColumn.setSortable(true);
		reasonColumn.setCellStyleNames("tableBody");
		sortHandler.setComparator(reasonColumn, new Comparator<RoleAssignmentSummaryPojo>() {
			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
				return o1.getRoleAssignment().getReason().compareTo(o2.getRoleAssignment().getReason());
			}
		});
		centralAdminListTable.addColumn(reasonColumn, "Reason");


		Column<RoleAssignmentSummaryPojo, String> effectiveDateColumn = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				if (object.getRoleAssignment().getEffectiveDate() != null) {
					return dateFormat.format(object.getRoleAssignment().getEffectiveDate());
				}
				else {
					return "Unknown";
				}
			}
		};
//		effectiveDateColumn.setSortable(true);
//		sortHandler.setComparator(effectiveDateColumn, new Comparator<RoleAssignmentSummaryPojo>() {
//			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
//				if (o1.getRoleAssignment().getEffectiveDate() != null && o2.getRoleAssignment().getEffectiveDate() != null) {
//					return o1.getRoleAssignment().getEffectiveDate().compareTo(o2.getRoleAssignment().getEffectiveDate());
//				}
//				else {
//					return 0;
//				}
//			}
//		});
		centralAdminListTable.addColumn(effectiveDateColumn, "Effective Date");

		Column<RoleAssignmentSummaryPojo, String> expirationDate = 
				new Column<RoleAssignmentSummaryPojo, String> (new TextCell()) {

			@Override
			public String getValue(RoleAssignmentSummaryPojo object) {
				if (object.getRoleAssignment().getExpirationDate() != null) {
					return dateFormat.format(object.getRoleAssignment().getExpirationDate());
				}
				else {
					return "Unknown";
				}
			}
		};
//		effectiveDateColumn.setSortable(true);
//		sortHandler.setComparator(effectiveDateColumn, new Comparator<RoleAssignmentSummaryPojo>() {
//			public int compare(RoleAssignmentSummaryPojo o1, RoleAssignmentSummaryPojo o2) {
//				if (o1.getRoleAssignment().getEffectiveDate() != null && o2.getRoleAssignment().getEffectiveDate() != null) {
//					return o1.getRoleAssignment().getEffectiveDate().compareTo(o2.getRoleAssignment().getEffectiveDate());
//				}
//				else {
//					return 0;
//				}
//			}
//		});
		centralAdminListTable.addColumn(expirationDate, "Expiration Date");
	}
	
	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		
		
	}

	@Override
	public void removeCentralAdminFromView(RoleAssignmentSummaryPojo centralAdmin) {
		dataProvider.getList().remove(centralAdmin);
	}

	@Override
	public void initPage() {
//		centralAdminIdTB.setText("");
//		centralAdminIdTB.getElement().setPropertyString("placeholder", "enter filter text");
	}

	@Override
	public void setMyNetIdURL(String url) {
		String intro = introBodyHTML.
				getHTML().
				replace("MY_NET_ID_URL", url);
		introBodyHTML.setHTML(intro);
	}

	@Override
	public void disableButtons() {
		
		
	}

	@Override
	public void enableButtons() {
		
		
	}

	@Override
	public void applyNetworkAdminMask() {
		
		
	}

}
