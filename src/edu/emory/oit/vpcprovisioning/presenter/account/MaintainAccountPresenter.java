package edu.emory.oit.vpcprovisioning.presenter.account;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import edu.emory.oit.vpcprovisioning.client.ClientFactory;
import edu.emory.oit.vpcprovisioning.client.VpcProvisioningService;
import edu.emory.oit.vpcprovisioning.client.event.ActionEvent;
import edu.emory.oit.vpcprovisioning.client.event.ActionNames;
import edu.emory.oit.vpcprovisioning.presenter.PresenterBase;
import edu.emory.oit.vpcprovisioning.shared.AccountNotificationPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountNotificationQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountNotificationQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.AccountPojo;
import edu.emory.oit.vpcprovisioning.shared.Constants;
import edu.emory.oit.vpcprovisioning.shared.DirectoryPersonPojo;
import edu.emory.oit.vpcprovisioning.shared.PropertyPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentPojo;
import edu.emory.oit.vpcprovisioning.shared.RoleAssignmentSummaryPojo;
import edu.emory.oit.vpcprovisioning.shared.SecurityRiskDetectionPojo;
import edu.emory.oit.vpcprovisioning.shared.SecurityRiskDetectionQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.SecurityRiskDetectionQueryResultPojo;
import edu.emory.oit.vpcprovisioning.shared.SpeedChartPojo;
import edu.emory.oit.vpcprovisioning.shared.SpeedChartQueryFilterPojo;
import edu.emory.oit.vpcprovisioning.shared.UserAccountPojo;

public class MaintainAccountPresenter extends PresenterBase implements MaintainAccountView.Presenter {
	private final ClientFactory clientFactory;
	private EventBus eventBus;
	private String accountId;
	private AccountPojo account;
	private String awsAccountsURL = "Cannot retrieve AWS Accounts URL";
	private String awsBillingManagementURL = "Cannot retrieve AWS Billing Management URL";
	private SpeedChartPojo speedType;
	private UserAccountPojo userLoggedIn;
	private DirectoryPersonPojo directoryPerson;
	private List<RoleAssignmentSummaryPojo> accountRoleAssignmentSummaries = new java.util.ArrayList<RoleAssignmentSummaryPojo>();
	private AccountNotificationQueryFilterPojo filter;
	PropertyPojo selectedProperty;
	private boolean isCimp=false;

	/**
	 * Indicates whether the activity is editing an existing case record or creating a
	 * new case record.
	 */
	private boolean isEditing;

	/**
	 * For creating a new ACCOUNT.
	 */
	public MaintainAccountPresenter(ClientFactory clientFactory) {
		this.isEditing = false;
		this.account = null;
		this.accountId = null;
		this.clientFactory = clientFactory;
		clientFactory.getMaintainAccountView().setPresenter(this);
	}

	/**
	 * For editing an existing ACCOUNT.
	 */
	public MaintainAccountPresenter(ClientFactory clientFactory, AccountPojo account) {
		this.isEditing = true;
		this.accountId = account.getAccountId();
		this.clientFactory = clientFactory;
		this.account = account;
		clientFactory.getMaintainAccountView().setPresenter(this);
	}

	@Override
	public String mayStop() {
		
		return null;
	}

	@Override
	public void start(EventBus eventBus) {
		getView().applyAWSAccountAuditorMask();
		getView().setFieldViolations(false);
		getView().resetFieldStyles();
		getView().hideFilteredStatus();
		this.eventBus = eventBus;
		setReleaseInfo(clientFactory);
		getView().showPleaseWaitPanel("Retrieving Account details, please wait...");
//		getView().showPleaseWaitDialog("Retrieving Account details, please wait...");
		getView().disableAdminMaintenance();

		if (accountId == null) {
			clientFactory.getShell().setSubTitle("Create Account");
			startCreate();
		} 
		else {
			clientFactory.getShell().setSubTitle("Edit Account");
			startEdit();
			// get latest version of the account from the server
			AsyncCallback<AccountPojo> acct_cb = new AsyncCallback<AccountPojo>() {
				@Override
				public void onFailure(Throwable caught) {
					getView().hidePleaseWaitDialog();
					getView().hidePleaseWaitPanel();
					getView().disableAdminMaintenance();
					GWT.log("Exception retrieving account details", caught);
					getView().showMessageToUser("There was an exception on the " +
							"server retrieving the details for this account.  Message " +
							"from server is: " + caught.getMessage());
				}

				@Override
				public void onSuccess(AccountPojo result) {
					account = result;
				}
			};
			VpcProvisioningService.Util.getInstance().getAccountById(accountId, acct_cb);
		}

		// indicate whether or not this is a CIMP instance of the Console
		AsyncCallback<Boolean> cimp_cb = new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(Boolean result) {
				isCimp = result;
				getView().setCimpInstance(result);
			}
		};
		VpcProvisioningService.Util.getInstance().isCimpInstance(cimp_cb);
		
		// set the financial account label if relevant
		AsyncCallback<String> finLabel_cb = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(String result) {
				if (result != null) {
					getView().setFinancialAccountFieldLabel(result);
				}
			}
		};
		VpcProvisioningService.Util.getInstance().getFinancialAccountFieldLabel(finLabel_cb);

		// get awsAccountsURL and awsBillingManagementURL in parallel
		AsyncCallback<String> accountsUrlCB = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("accounts URL from server: " + result);
				awsAccountsURL = result;
				getView().setAwsAccountsURL(awsAccountsURL);
			}
		};
		VpcProvisioningService.Util.getInstance().getAwsAccountsURL(accountsUrlCB);

		AsyncCallback<String> billingUrlCB = new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(String result) {
				GWT.log("billing URL from server: " + result);
				awsBillingManagementURL = result;
				getView().setAwsBillingManagementURL(awsBillingManagementURL);
			}
		};
		VpcProvisioningService.Util.getInstance().getAwsBillingManagementURL(billingUrlCB);
		
		AsyncCallback<UserAccountPojo> userCallback = new AsyncCallback<UserAccountPojo>() {

			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				getView().disableAdminMaintenance();
				GWT.log("Exception retrieving user logged in", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving the user logged in.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(final UserAccountPojo user) {
				userLoggedIn = user;
				getView().setUserLoggedIn(user);
				if (isEditing) {
					refreshAccountNotificationList(user);
				}
				else {
					getView().setAccountNotifications(Collections.<AccountNotificationPojo> emptyList());
				}
				List<String> filterTypeItems = new java.util.ArrayList<String>();
				filterTypeItems.add(Constants.USR_NOT_FILTER_SUBJECT);
				filterTypeItems.add(Constants.USR_NOT_FILTER_REF_ID);
				getView().setFilterTypeItems(filterTypeItems);
				
				AsyncCallback<List<String>> complianceType_cb = new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						GWT.log("Exception retrieving Compliance Class types", caught);
						getView().showMessageToUser("There was an exception on the " +
								"server retrieving a list of Compliance Class types.  Message " +
								"from server is: " + caught.getMessage());
					}

					@Override
					public void onSuccess(List<String> complianceClassTypes) {
						getView().setComplianceClassItems(complianceClassTypes);
					}
				};
				GWT.log("getting comliance class types");
				VpcProvisioningService.Util.getInstance().getComplianceClassItems(complianceType_cb);


				AsyncCallback<List<String>> callback = new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						getView().hidePleaseWaitDialog();
						getView().hidePleaseWaitPanel();
						getView().disableAdminMaintenance();
						GWT.log("Exception retrieving e-mail types", caught);
						getView().showMessageToUser("There was an exception on the " +
								"server retrieving e-mail types.  Message " +
								"from server is: " + caught.getMessage());
					}

					@Override
					public void onSuccess(List<String> result) {
						getView().setEmailTypeItems(result);

						getView().initPage();
						getView().setFieldViolations(false);
						getView().setInitialFocus();
						
						if (isEditing) {
							getRoleAssignmentsForAccount();
						}
						
						// apply authorization mask
						if (user.isCentralAdmin()) {
							GWT.log("[maintain account] " + userLoggedIn.getPersonalName().toString() + 
								" IS a CentrlAdmin");
							getView().applyCentralAdminMask();
							getView().enableAdminMaintenance();
						}
						else if (account != null) {
							if (user.isAdminForAccount(account.getAccountId())) {
								GWT.log("[maintain account] " + userLoggedIn.getPersonalName().toString() + 
									" IS an AccountAdmin for account: " + account.getAccountId());
								getView().applyAWSAccountAdminMask();
								getView().enableAdminMaintenance();
							}
							else if (user.isAuditorForAccount(account.getAccountId())) {
								GWT.log("[maintain account] " + userLoggedIn.getPersonalName().toString() + 
									" IS an AccountAuditor for account: " + account.getAccountId());
								getView().applyAWSAccountAuditorMask();
								getView().disableAdminMaintenance();
							}
						}
						
						if (!isEditing) {
							getView().hidePleaseWaitDialog();
							getView().hidePleaseWaitPanel();
						}
					}
				};
				VpcProvisioningService.Util.getInstance().getEmailTypeItems(callback);
			}
		};
		VpcProvisioningService.Util.getInstance().getUserLoggedIn(false, userCallback);
	}

	private void startCreate() {
		GWT.log("Maintain account: create");
		isEditing = false;
		getView().setEditing(false);
		account = new AccountPojo();
	}

	private void startEdit() {
		GWT.log("Maintain account: edit");
		isEditing = true;
		getView().setEditing(true);
		// Lock the display until the account is loaded.
		getView().setLocked(true);
	}

	@Override
	public void stop() {
		eventBus = null;
		clientFactory.getMaintainAccountView().setLocked(false);
	}

	@Override
	public void setInitialFocus() {
		getView().setInitialFocus();
	}

	@Override
	public Widget asWidget() {
		return getView().asWidget();
	}

	@Override
	public void deleteAccount() {
		if (isEditing) {
			doDeleteAccount();
		} else {
			doCancelAccount();
		}
	}

	/**
	 * Cancel the current case record.
	 */
	private void doCancelAccount() {
		ActionEvent.fire(eventBus, ActionNames.ACCOUNT_EDITING_CANCELED);
	}

	/**
	 * Delete the current case record.
	 */
	private void doDeleteAccount() {
		if (account == null) {
			return;
		}

		// TODO Delete the account on server then fire onAccountDeleted();
	}

	@Override
	public void saveAccount() {
		getView().showPleaseWaitDialog("Saving account...");
		List<Widget> fields = getView().getMissingRequiredFields();
		if (fields != null && fields.size() > 0) {
			getView().setFieldViolations(true);
			getView().applyStyleToMissingFields(fields);
			getView().hidePleaseWaitDialog();
			getView().hidePleaseWaitPanel();
			getView().showMessageToUser("Please provide data for the required fields.");
			return;
		}
		else {
			getView().setFieldViolations(false);
			getView().resetFieldStyles();
		}
		AsyncCallback<AccountPojo> callback = new AsyncCallback<AccountPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Exception saving the Account", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server saving the Account.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(AccountPojo result) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				ActionEvent.fire(eventBus, ActionNames.ACCOUNT_SAVED, account);
			}
		};
		if (!this.isEditing) {
			// it's a create
			VpcProvisioningService.Util.getInstance().createAccount(account, callback);
		}
		else {
			// it's an update
			VpcProvisioningService.Util.getInstance().updateAccount(account, callback);
		}
	}

	@Override
	public AccountPojo getAccount() {
		return this.account;
	}

	@Override
	public boolean isValidAccountId(String value) {
		
		return false;
	}

	@Override
	public boolean isValidAccountName(String value) {
		
		return false;
	}

	public MaintainAccountView getView() {
		return clientFactory.getMaintainAccountView();
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	public void setAccount(AccountPojo account) {
		this.account = account;
	}

	@Override
	public void setSpeedChartStatusForKeyOnWidget(final String key, final Widget w, final boolean confirmSpeedType) {
		GWT.log("[setSpeedChartStatusForKeyOnWidget] validating speed type: " + key);
		AsyncCallback<SpeedChartPojo> callback = new AsyncCallback<SpeedChartPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Server exception validating speedtype", caught);
				w.setTitle("Server exception validating speedtype");
				getView().setSpeedTypeStatus("Server exception validating speedtype");
				getView().setSpeedTypeColor(Constants.COLOR_RED);
			}

			@Override
			public void onSuccess(SpeedChartPojo scp) {
				if (scp == null) {
					w.setTitle("Invalid account number (" + key + "), can't validate this number");
					w.getElement().getStyle().setBackgroundColor("#efbebe");
					getView().setSpeedTypeStatus("<b>Invalid account</b>");
					getView().setSpeedTypeColor(Constants.COLOR_RED);
					getView().setFieldViolations(true);
				}
				else {
//				    DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
					GWT.log("[setSpeedChartStatusForKeyOnWidget] got a speed chart.");
					speedType = scp;
					String deptId = scp.getDepartmentId();
					String deptDesc = scp.getDepartmentDescription();
					String desc = scp.getDescription();
				    String euValidityDesc = scp.getEuValidityDescription();
				    String statusDescString = euValidityDesc + "\n" + 
				    		deptId + " | " + deptDesc + "\n" +
				    		desc;
				    String statusDescHTML = "<b>" + euValidityDesc + "<br>" + 
				    		deptId + " | " + deptDesc + "<br>" +
				    		desc + "<b>";
					w.setTitle(statusDescString);
					getView().setSpeedTypeStatus(statusDescHTML);
					GWT.log("[setSpeedChartStatusForKeyOnWidget] set speed type status html.");
					if (scp.getValidCode().equalsIgnoreCase(Constants.SPEED_TYPE_VALID)) {
						getView().setSpeedTypeColor(Constants.COLOR_GREEN);
						w.getElement().getStyle().setBackgroundColor(null);
						getView().setFieldViolations(false);
						if (confirmSpeedType) {
							didConfirmSpeedType();
						}
					}
					else if (scp.getValidCode().equalsIgnoreCase(Constants.SPEED_TYPE_INVALID)) {
						getView().setSpeedTypeColor(Constants.COLOR_RED);
						w.getElement().getStyle().setBackgroundColor(Constants.COLOR_INVALID_FIELD);
						getView().setFieldViolations(true);
					}
					else {
						getView().setSpeedTypeColor(Constants.COLOR_ORANGE);
						w.getElement().getStyle().setBackgroundColor(Constants.COLOR_FIELD_WARNING);
						if (confirmSpeedType) {
							didConfirmSpeedType();
						}
					}
				}
			}
		};
		if (key != null && key.length() > 0) {
			SpeedChartQueryFilterPojo filter = new SpeedChartQueryFilterPojo();
			filter.getSpeedChartKeys().add(key);
			VpcProvisioningService.Util.getInstance().getSpeedChartForFinancialAccountNumber(key, callback);
		}
		else {
			GWT.log("null key, can't validate yet");
		}
	}

	@Override
	public void setSpeedChartStatusForKey(String key, Label label, boolean confirmSpeedType) {
		GWT.log("[setSpeedChartStatusForKey] validating speed type: " + key);
		// null check / length
		if (key == null || key.length() != 10) {
			getView().setSpeedTypeStatus("<b>Invalid length</b>");
			getView().setSpeedTypeColor(Constants.COLOR_RED);
			getView().setFieldViolations(true);
			return;
		}
		// TODO: numeric characters
		
		setSpeedChartStatusForKeyOnWidget(key, getView().getSpeedTypeWidget(), confirmSpeedType);
	}

	@Override
	public boolean didConfirmSpeedType() {
		if (this.account != null && 
			this.account.getSpeedType() != null && 
			this.account.getAccountId() != null) {
			
			boolean confirmed = Window.confirm("Are you sure you want to use this SpeedType?  "
					+ "NOTE:  Using an invalid SpeedType is a violation of " + 
					getView().getAppShell().getSiteName() + "'s Terms of Use.");
			if (confirmed) {
				// TODO: log that the user acknowldged the speed type (on the server)
				DateTimeFormat dateFormat = DateTimeFormat.getFormat("MM-dd-yyyy HH:mm:ss:SSS zzz");
				String msg = "User " + this.userLoggedIn.getPublicId() + " acknowledged "
						+ "the SpeedType " + this.account.getSpeedType() 
						+ " for account " + this.account.getAccountId() 
						+ " (" + this.account.getAccountName() + ") "  
						+ "is the correct SpeedType for this account at: " + new Date();
				this.logMessageOnServer(msg);
				getView().showMessageToUser("Logged " + msg);
				getView().setSpeedTypeConfirmed(true);
				return true;
			}
		}
		// user decided they didn't want to use this speed type, or the account hasn't been 
		// entered yet
		getView().setSpeedTypeConfirmed(false);
		return false;
	}

	@Override
	public SpeedChartPojo getSpeedType() {
		return speedType;
	}

	@Override
	public DirectoryPersonPojo getDirectoryPerson() {
		return directoryPerson;
	}

	@Override
	public void setDirectoryPerson(DirectoryPersonPojo directoryPerson) {
		GWT.log("[presenter] setting directory person to: " + directoryPerson.toString());
		this.directoryPerson = directoryPerson;
	}

	@Override
	public void addDirectoryPersonInRoleToAccount(final String roleName) {
		AsyncCallback<RoleAssignmentPojo> raCallback = new AsyncCallback<RoleAssignmentPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				getView().showMessageToUser("There was an exception on the " +
						"server creating the role assignment.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(final RoleAssignmentPojo roleAssignment) {
				// then, tell the view to refresh it's role list
				GWT.log("start - onSuccess...");
				RoleAssignmentSummaryPojo ra_summary = new RoleAssignmentSummaryPojo();
				ra_summary.setDirectoryPerson(directoryPerson);
				ra_summary.setRoleAssignment(roleAssignment);
				accountRoleAssignmentSummaries.add(ra_summary);
				String roleName = getSimpleRoleNameFromRoleDN(ra_summary.getRoleAssignment().getRoleDN());
				GWT.log("start - getView().addRoleAssignment...");
				getView().addRoleAssignment(accountRoleAssignmentSummaries.size() - 1, directoryPerson.getFullName(), 
						directoryPerson.getEmail().getEmailAddress(), roleName, 
						directoryPerson.toString() + " " + ra_summary.getRoleAssignment().getRoleDN());
				GWT.log("done - getView();.addRoleAssignment...");
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
			}
		};
		// now, create the role assignment and add the role assignment to the account
		getView().showPleaseWaitDialog("Creating Role Assignment with the IDM service...");
		VpcProvisioningService.Util.getInstance().createRoleAssignmentForPersonInAccount(directoryPerson.getKey(), account.getAccountId(), roleName, raCallback);
	}

	@Override
	public void getRoleAssignmentsForAccount() {
		
		AsyncCallback<List<RoleAssignmentSummaryPojo>> callback = new AsyncCallback<List<RoleAssignmentSummaryPojo>>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				GWT.log("Exception retrieving Administrators", caught);
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving Administrators.  Message " +
						"from server is: " + caught.getMessage());
//				getView().enableAdminMaintenance();
			}

			@Override
			public void onSuccess(List<RoleAssignmentSummaryPojo> result) {
				accountRoleAssignmentSummaries = result;
				// add each role assignment summary to the view
				for (int i=0; i<result.size(); i++) {
					RoleAssignmentSummaryPojo ra_summary = result.get(i);
					String roleName = getSimpleRoleNameFromRoleDN(ra_summary.getRoleAssignment().getRoleDN());
					getView().addRoleAssignment(i, ra_summary.getDirectoryPerson().getFullName(), 
							ra_summary.getDirectoryPerson().getEmail().getEmailAddress(), roleName,  
							ra_summary.getDirectoryPerson().toString());
				}
//				getView().enableAdminMaintenance();
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
			}
		};
		getView().showPleaseWaitDialog("Retrieving Role Assignments from the IDM service...");
		VpcProvisioningService.Util.getInstance().getRoleAssignmentsForAccount(account.getAccountId(), callback);
	}
	
	private String getSimpleRoleNameFromRoleDN(String roleDn) {
		String roleName = null;
		if (roleDn == null) {
			GWT.log("null roleDn");
			return "Unknown";
		}
		if (roleDn.indexOf(Constants.ROLE_NAME_RHEDCLOUD_AWS_ADMIN) >= 0) {
			roleName = Constants.STATIC_TEXT_ADMINISTRATOR;
		}
		else if (roleDn.indexOf(Constants.ROLE_NAME_RHEDCLOUD_AUDITOR) >= 0) {
			roleName = Constants.STATIC_TEXT_AUDITOR;
		}
		else {
			roleName = "Unknown";
		}
		return roleName;
	}

	@Override
	public List<RoleAssignmentSummaryPojo> getRoleAssignmentSummaries() {
		return accountRoleAssignmentSummaries;
	}

	@Override
	public void removeRoleAssignmentFromAccount(String accountId, final RoleAssignmentSummaryPojo roleAssignmentSummary) {
		AsyncCallback<Void> callback = new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error removing person from role", caught);
				getView().hidePleaseWaitDialog();
				getView().showMessageToUser("Error removing person from role.  Error is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Void result) {
				accountRoleAssignmentSummaries.remove(roleAssignmentSummary);
				getView().hidePleaseWaitDialog();
				getView().showMessageToUser(roleAssignmentSummary.getDirectoryPerson().getFullName() + 
					" was successfully removed from Role " + roleAssignmentSummary.getRoleAssignment().getRoleDN());
			}
		};
		getView().showPleaseWaitDialog("Deleting Role Assignment...");
		VpcProvisioningService.Util.getInstance().removeRoleAssignmentFromAccount(accountId, 
				roleAssignmentSummary.getRoleAssignment(), callback);
	}

	@Override
	public void saveNotification(AccountNotificationPojo selected) {
		
		
	}

	@Override
	public void deleteNotification(AccountNotificationPojo selected) {
		
		
	}

	@Override
	public void showSrdForAccountNotification(final AccountNotificationPojo selected) {
		getView().showPleaseWaitDialog("Retrieving Security Risk Detection from the SRD service...");
		AsyncCallback<SecurityRiskDetectionQueryResultPojo> cb = new AsyncCallback<SecurityRiskDetectionQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving the Security Risk Detection.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(SecurityRiskDetectionQueryResultPojo result) {
				getView().hidePleaseWaitDialog();
				getView().hidePleaseWaitPanel();
				if (result.getResults().size() > 0) {
					SecurityRiskDetectionPojo srd = result.getResults().get(0);
					ActionEvent.fire(getEventBus(), ActionNames.VIEW_SRD_FOR_ACCOUNT_NOTIFICATION, srd, selected);
				}
				else {
					getView().hidePleaseWaitDialog();
					getView().hidePleaseWaitPanel();
					getView().showMessageToUser("An unexpected result was returned from the server." +
							"  Could not find any Security Risk Detection objects for the reference id: " + 
							selected.getReferenceid() +
							" this is likely a data issue on the backend.");
				}
			}
			
		};
		SecurityRiskDetectionQueryFilterPojo filter = new SecurityRiskDetectionQueryFilterPojo();
		filter.setSecurityRiskDetectionId(selected.getReferenceid());
		VpcProvisioningService.Util.getInstance().getSecurityRiskDetectionsForFilter(filter, cb);
	}

	@Override
	public void refreshAccountNotificationList(UserAccountPojo user) {
		AsyncCallback<AccountNotificationQueryResultPojo> acct_not_cb = new AsyncCallback<AccountNotificationQueryResultPojo>() {
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Exception retrieving account notifications", caught);
				getView().hidWaitForNotificationsDialog();
				getView().showMessageToUser("There was an exception on the " +
						"server retrieving the notifications for this account.  "
						+ "Processing will continue.  Message " +
						"from server is: " + caught.getMessage());
			}

			@Override
			public void onSuccess(AccountNotificationQueryResultPojo result) {
				getView().setAccountNotifications(result.getResults());
				getView().hidWaitForNotificationsDialog();
			}
		};
		getView().showWaitForNotificationsDialog("Retreiving Account Notifications (potential long running task)...");
		// get account notifications for this account
		if (filter == null) {
			filter = new AccountNotificationQueryFilterPojo();
			filter.setAccountId(accountId);
			filter.setUseQueryLanguage(true);
			filter.setMaxRows(100);
		}
		VpcProvisioningService.Util.getInstance().getAccountNotificationsForFilter(filter, acct_not_cb);
	}

	@Override
	public void setSelectedProperty(PropertyPojo prop) {
		this.selectedProperty = prop;
	}

	@Override
	public PropertyPojo getSelectedProperty() {
		return this.selectedProperty;
	}

	@Override
	public void updateProperty(PropertyPojo prop) {
		int index = -1;
		propertyLoop: for (int i=0; i<account.getProperties().size(); i++) {
			PropertyPojo tpp = account.getProperties().get(i);
			if (tpp.getName().equalsIgnoreCase(prop.getName())) {
				index = i;
				break propertyLoop;
			}
		}
		if (index >= 0) {
			GWT.log("Updating property: " + index);
			account.getProperties().remove(index);
			account.getProperties().add(prop);
		}
		else {
			GWT.log("Counldn't find a property to update...problem");
		}
	}

	@Override
	public void filterBySubject(String subject) {
		getView().showFilteredStatus();
		filter = new AccountNotificationQueryFilterPojo();
		filter.setAccountId(accountId);
		filter.setFuzzyFilter(true);
		filter.setSubject(subject);
		filter.setUseQueryLanguage(true);
		this.refreshAccountNotificationList(userLoggedIn);
	}

	@Override
	public void filterByReferenceId(String referencId) {
		getView().showFilteredStatus();
		filter = new AccountNotificationQueryFilterPojo();
		filter.setAccountId(accountId);
		filter.setFuzzyFilter(true);
		filter.setReferenceId(referencId);
		filter.setUseQueryLanguage(true);
		this.refreshAccountNotificationList(userLoggedIn);
	}

	@Override
	public void setAccountNotificationFilter(AccountNotificationQueryFilterPojo filter) {
		this.filter = filter;
	}

	@Override
	public AccountNotificationQueryFilterPojo getAccountNotificationFilter() {
		return this.filter;
	}
}
