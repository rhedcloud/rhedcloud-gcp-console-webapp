package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class UserActionQueryFilterPojo extends SharedObject implements IsSerializable, QueryFilter  {
	/*
		<!ELEMENT UserActionQuerySpecification (
		Comparison*, 
		QueryLanguage?, 
		UserId, 
		Action)>
	 */
	
	String userId;
	String action;
	
	public UserActionQueryFilterPojo() {
		
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

}
