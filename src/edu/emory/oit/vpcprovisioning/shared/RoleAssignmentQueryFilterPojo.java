package edu.emory.oit.vpcprovisioning.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class RoleAssignmentQueryFilterPojo extends SharedObject implements IsSerializable, QueryFilter  {
	//RoleDN?, IdentityType?, DirectAssignOnly?, UserDN?
	String roleDN;
	String identityType;
	boolean directAssignOnly;
	String userDN;
	UserAccountPojo userLoggedIn;

	public RoleAssignmentQueryFilterPojo() {
		
	}

	public String getRoleDN() {
		return roleDN;
	}

	public void setRoleDN(String roleDN) {
		this.roleDN = roleDN;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getUserDN() {
		return userDN;
	}

	public void setUserDN(String userDN) {
		this.userDN = userDN;
	}

	public boolean isDirectAssignOnly() {
		return directAssignOnly;
	}

	public void setDirectAssignOnly(boolean directAssignOnly) {
		this.directAssignOnly = directAssignOnly;
	}

	@Override
	public String toString() {
		return "roleDN: " + roleDN + ", identityType: " + identityType + ", userDN: " + userDN;
	}

	public UserAccountPojo getUserLoggedIn() {
		return userLoggedIn;
	}

	public void setUserLoggedIn(UserAccountPojo userLoggedIn) {
		this.userLoggedIn = userLoggedIn;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

}
