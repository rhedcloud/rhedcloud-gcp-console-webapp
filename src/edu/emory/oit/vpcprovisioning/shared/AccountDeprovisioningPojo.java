package edu.emory.oit.vpcprovisioning.shared;

import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

@SuppressWarnings("serial")
public class AccountDeprovisioningPojo extends SharedObject implements IsSerializable, Comparable<AccountDeprovisioningPojo> {

	/*
	<!ELEMENT AccountDeprovisioning (
		DeprovisioningId, 
		AccountDeprovisioningRequisition, 
		Status, 
		DeprovisioningResult?, 
		ActualTime?, 
		AnticipatedTime?, 
		DeprovisioningStep*, 
		CreateUser, 
		CreateDatetime, 
		LastUpdateUser?, 
		LastUpdateDatetime?)>
	 */
	String deprovisioningId;
	AccountDeprovisioningRequisitionPojo requisition;
	String status;
	String deprovisioningResult;
	String actualTime;
	String anticipatedTime;
	List<ProvisioningStepPojo> deprovisioningSteps = new java.util.ArrayList<ProvisioningStepPojo>();	
	AccountDeprovisioningPojo baseline;
	
	public int getTotalStepCount() {
		return deprovisioningSteps.size();
	}
	
	public int getCompletedSuccessfulCount() {
		// if status is 'complete' and stepResult is 'success' increment counter
		int cnt = 0;
		for (ProvisioningStepPojo step : deprovisioningSteps) {
			if (step.getStatus() != null) {
				if (step.getStatus().equalsIgnoreCase(Constants.PROVISIONING_STEP_STATUS_COMPLETED)) {
					if (step.getStepResult() != null) {
						if (step.getStepResult().equalsIgnoreCase(Constants.VPCP_STEP_RESULT_SUCCESS)) {
							cnt++;
						}
					}
				}
			}
		}
		return cnt;
	}
	
	public static final ProvidesKey<AccountDeprovisioningPojo> KEY_PROVIDER = new ProvidesKey<AccountDeprovisioningPojo>() {
		@Override
		public Object getKey(AccountDeprovisioningPojo item) {
			return item == null ? null : item.getDeprovisioningId();
		}
	};

	public AccountDeprovisioningPojo() {
		
	}

	@Override
	public int compareTo(AccountDeprovisioningPojo o) {
		Date c1 = o.getCreateTime();
		Date c2 = this.getCreateTime();
		if (c1 == null || c2 == null) {
			return 0;
		}
		return c1.compareTo(c2);
	}

	public String getDeprovisioningId() {
		return deprovisioningId;
	}

	public void setDeprovisioningId(String provioningId) {
		this.deprovisioningId = provioningId;
	}

	public AccountDeprovisioningRequisitionPojo getRequisition() {
		return requisition;
	}

	public void setRequisition(AccountDeprovisioningRequisitionPojo requisition) {
		this.requisition = requisition;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeprovisioningResult() {
		return deprovisioningResult;
	}

	public void setDeprovisioningResult(String provisioningResult) {
		this.deprovisioningResult = provisioningResult;
	}

	public String getActualTime() {
		return actualTime;
	}

	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public String getAnticipatedTime() {
		return anticipatedTime;
	}

	public void setAnticipatedTime(String anticipatedTime) {
		this.anticipatedTime = anticipatedTime;
	}

	public List<ProvisioningStepPojo> getDeprovisioningSteps() {
		return deprovisioningSteps;
	}

	public void setDeprovisioningSteps(List<ProvisioningStepPojo> provisioningSteps) {
		this.deprovisioningSteps = provisioningSteps;
	}

	public AccountDeprovisioningPojo getBaseline() {
		return baseline;
	}

	public void setBaseline(AccountDeprovisioningPojo baseline) {
		this.baseline = baseline;
	}

}
